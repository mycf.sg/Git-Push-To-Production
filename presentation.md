---
title: git push -f origin production - MCF Edition
author: Ryan Goh  slides:gitlab.com/mycf.sg/Git-Push-To-Production
patat:
  wrap: true
  margins:
    top: 1
    left: 1
  theme:
    strong: [bold]
    imageTarget: [onDullWhite, vividRed]
  images:
    backend: 'w3m'
...

# MyCareersFuture

Git Push To Production

---

![mcf](images/mcf.jpg)

---

# Content

- Orientation
    - Tech Stack
    - Main services (Squad Structure)
    - Git Practices

- Pipeline Flow
    - Versioning
    - Dependencies
    - Test
    - Build
    - Deploy

- Production
    - Processes (Business & Tech)

---

# Tech Stack

## Main

||||
|-|--|--|-|
|Javascript (Typescript)| |Kotlin|
|Golang|||
|Gitlab|||
|Docker| | Kubernetes|

## Miscellaneous

Rust

---


# Main Services

<!--
diagram-src/services.puml
-->

```txt
      ,------------.
      |ui-jobseeker| (Jobseeker Squad)
      |------------|
      |------------|
      `------------'
            |
            |
        ,-------.   ,-----------.
        |api-job|   |api-profile|
        |-------|---|-----------|
        |-------|   |-----------|
        `-------'   `-----------'
            |
      ,-----------.
      |ui-employer|  (Employer Squad)
      |-----------|
      |-----------|
      `-----------'
```

---

# Git Practices

Feature branching

Push Rules

```sh
"^((docs|feat|fix|perf|refactor|style|test|chore):).+$"
```

Merge requests

- 2 Approvals required before merging to master

Trunk based with feature branches

- Only __master__ branch
- version tag commits that are releasable

---

# Basic pipeline flow

<!--
diagram-src/basic-flow.puml
-->

```txt

,----------.  ,------------.   ,----------.   ,-----.
|versioning|  |dependencies|   |Unit tests|   |build|
|----------|  |------------|   |----------|   |-----|
|----------|--|------------|---|----------|---|-----|
`----------'  `------------'   `----------'   `-----'
                                                  |
                                                  |
       ,----------.   ,-----------------.   ,---------.
       |deploy UAT|   |integration tests|   |deploy QA|
       |----------|---|-----------------|---|---------|
       |----------|   |-----------------|   |---------|
       `----------'   `-----------------'   `---------'

```

---

# Versioning

- Semantic Versioning

- Every master pipeline will increment it (0.0.1 -> 0.0.2)

- Version tag will follow code through for all respective resources (artifacts, production)

---

# Dependencies

- Pulling of dependencies (npm, go mod)

- cache dependencies (for faster subsequent jobs)

---

# Test

## Merge Requests
- Unit Test
    - Jest for UI repos
    - Mocha for API repos
- lint
    - eslint

---
# Build

- docker images only

- image tagged with version number from VERSIONING stage
    - e.g api-profile:0.0.1

---

```Dockerfile
# api-profile
FROM node:10.17.0-alpine3.10 AS base
RUN apk update --no-cache
RUN apk upgrade --no-cache
ENV TZ="UTC"
# - - -
FROM base AS development
ENV NODE_ENV development
RUN apk add --no-cache bash
RUN apk add --no-cache git
RUN apk add --no-cache make
RUN apk add --no-cache python
RUN apk add --no-cache g++
# - - -
FROM base AS production
ENV NODE_ENV production
RUN apk del --no-cache wget
RUN rm -rf /usr/bin/wget
WORKDIR /app
COPY ./package.json /app/package.json
COPY ./yarn.lock /app/yarn.lock
RUN yarn install --production --pure-lockfile --non-interactive \
  && yarn cache clean \
  && rm -rf /usr/local/share/.cache/yarn
COPY . /app
```

---

# Deploy

- GitOps
    - Argocd

    - Git repository represents the current state of the application deployed

    - Deployment stage is basically a git commit that contains the image tag changes
      - Create a branch with the required changes
      - Create merge request of the branch to master
      - Can be automated or upon approval

---

## Service pipeline

```yml
.deploy_script: &deploy_script
  script:
    - VERSION=$(cat .version)-${NODE_ENV}
    - printf -- "Updating ops-k8s repository\n"
    - |
      curl --fail -X POST \
        -F "token=${OPS_TRIGGER_TOKEN}" -F "ref=master" -F "variables[VERSION]=${VERSION}" \
        -F "variables[ENV]=${NODE_ENV}" -F "variables[SERVICE]=ui-employer" \
        https://gitlab.com/api/v4/projects/${OPS_TRIGGER_ID}/trigger/pipeline
  dependencies:
    - version
```

---

# k8s pipline

Create Branch

```sh
printf -- "Creating branch for ${BRANCH_NAME} in ${ENV}\n"
git clone git@gitlab.com:${CI_PROJECT_PATH}.git
.
.
git checkout -b ${BRANCH_NAME}
sed -i -r "s/(image:.*${IMAGE_NAME}:)([0-9]+.[0-9]+.[0-9]+?.*)/\1${VERSION}/" ./${CI_FOLDER}/${SERVICE}/${ENV}/*
git add ./${CI_FOLDER}/${SERVICE}/${ENV}
git commit -m "Updating deployment image tags in ./${CI_FOLDER}/${SERVICE}/${ENV} to ${VERSION}"
git push --set-upstream origin ${BRANCH_NAME}
```

---

# k8s pipeline

Create MR
```sh
BODY="{
  \"id\": ${CI_PROJECT_ID},
  \"source_branch\": \"${BRANCH_NAME}\",
  \"target_branch\": \"${DEFAULT_BRANCH}\",
  \"remove_source_branch\": true,
  \"title\": \"${MR_NAME} \#${CI_COMMIT_SHORT_SHA}\"
}";
printf -- "The following body to Gitlab API \n%s\n" "${BODY}"
printf -- "Calling gitlab API to open a new merge request...\n"
curl --silent -X POST "${HOST}/${CI_PROJECT_ID}/merge_requests" \
  --header "PRIVATE-TOKEN: ${GITLAB_API_PRIVATE_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "${BODY}";
printf -- "Opened a new merge request successfully: %s\n" "${MR_NAME}";
```

---

# k8s pipeline

Auto merge MR

```sh
printf -- "New version (%s) is higher or equal to currently deployed (%s)\nAuto merging this MR...\n" "${NEW_VERSION}" "${CURRENT_VERSION}";
printf -- "Sending api to %s" "${HOST}/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/merge"
curl -X PUT "${HOST}/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/merge" --silent --header "PRIVATE-TOKEN:${GITLAB_API_PRIVATE_TOKEN}";
```

---

# Integration Test
## Master (happens in between QA and UAT)

- Integration tests (UI Automation)
    - Cypress
    - Robot Framework (Legacy)
- Integration tests (3rd party)
    - Dredd
- Contract Test
    - Pact

---


# Production
## Processes

- Send email (Old)

- New flow with pivotal tracker
Below are the list of item scheduled for upcoming release.

**Jobseeker**:
#16866000 Enable Jobscan's suggestions to be printed

**Employer**:

**Bug**
#170350000 Bug - System Contact shouldn't be checked when SingPass user accesses the portal.

**Feature toggle config - PROD**:
1. resumeSectionNudge (jobseeker.json) - [set to false to hide static nudges]: value to be set for release: **true**

**Environment Variables**
*api-job*
- API_CONSTANT_URL = *no need to set* (because api-constant not ready yet)

```
**Version to be deployed**:
ui-jobseeker : 0.0.2017
ui-employer : 0.0.1014 (no change from prod)
api-Job : 0.0.1058
api-Profile : 0.0.559
```

---

# Production
## Technical

- Separate git repo

- AWS
    - Manually __`$ kubectl apply -f ./`__

- Nectar
    - Use mouse to click

---

# End

```sh
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 890 bytes | 890.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To gitlab.com:mycf.sg/Git-Push-To-Production.git
   6451584..4ec2acf  master -> master
```
