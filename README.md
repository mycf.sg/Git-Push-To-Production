# Git Push To Production

## prerequisites

This presentation requires the use of [https://github.com/jaspervdj/patat/](https://github.com/jaspervdj/patat/) to display the "slides" in terminal, recommended terminals are iterm2 or urxvt to display images

## Run

```sh
patat presentation.md
```

### Editing

```sh
patat presentation.md --watch
```
